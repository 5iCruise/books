## 豆瓣 2019 书单对应文件

书的内容简介可以在 [豆瓣 2019 年度榜单](https://book.douban.com/annual/2019?source=navigation) 中查阅。 

- 格式 `mobi`, `azw3`, `epub`, `pdf`
- 总大小 525M 

<p align="center"> <img width="666" alt="books-of-year" src="img/dir.png" style="border-radius: 20px"/><p>


-----

- Update 2020-Jan-07：添加《地球编年史》 pdf 全八册，840 M
- Update 2020-Jan-08：添加《Reader's Digest》2019 合集 [2019.01-2019.12] 421M






Contact: r2fscg@gmail.com

